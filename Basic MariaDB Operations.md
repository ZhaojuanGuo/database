## MariaDB installation
https://www.redhat.com/sysadmin/mysql-mariadb-introduction


## Create a table
### CREATE TABLE
MariaDB [test_result]> _**create table rdma_protocol ( protocol_id INT NOT NULL AUTO_INCREMENT, protocol_name VARCHAR(10), PRIMARY KEY (protocol_id) );**_
### CREATE TABLE ... LIKE
Use the LIKE clause instead of a full table definition to create a table with the same definition as another table, including columns, indexes, and table options. Foreign key definitions, as well as any DATA DIRECTORY or INDEX DIRECTORY table options specified on the original table, will not be created.
- MariaDB [test_result]> create table tc_librdmacm-utils like tc_iser;

## Alter a table
### Add a column
- MariaDB [test_result]> **_alter table rdma_protocol add desciption TEXT;_**
- DATETIME type, MariaDB [test_result]> _**alter table tc_iser add date_of_last_update DATETIME**;_
### Modify the length of a column
- MariaDB [test_result]> alter table tc_sanity modify driver varchar(30);
### Modify the name of a column
- MariaDB [test_result]> alter table tc_sanity change issue_line issue_link varchar(100);
### Update the data of a row
- MariaDB [test_result]> update tc_iser set log = "https://beaker.engineering.redhat.com/jobs/8581169" where driver = "qedr iw"; 
### Modify the order of columns
- MariaDB [test_result]> alter table tc_iser modify issue_id int auto_increment first;
- MariaDB [test_result]> alter table tc_iser modify issue_desc text after issue_link;
### Delete a column
- MariaDB [test_result]> alter table tc_librdmacm_utils drop column test_result;

## Insert data into a table
- MariaDB [test_result]> insert into rdma_protocol (protocol_name,desciption) values ('iWARP', 'Internet Wide Area Rdma Protocol'); 
- now() to get DATETIME, MariaDB [test_result]> insert into tc_iser (driver,compose,kernel,component,test_result,log,issue_link,date_of_last_update) values('qedr iw', 'RHEL-9.4.0-*',
'','','FAIL', '', 'Bug 2033521', _**now()**_);


## Delete data from a table
MariaDB [test_result]> _**delete from tc_pyverbs_tests where issue_link = 'RHEL-16769'**_

